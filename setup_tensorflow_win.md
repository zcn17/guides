# Guide to setup TensorFlow on Windows

This guide walks you through the setup of TensorFlow with GPU support on Windows.

The flow is
* **Install Visual Studio**
    * This is needed to build the CUDA Sample Codes.
* **Install CUDA Toolkit with Nvidia Driver**
* **Install cuDNN**
    * This is a GPU-accelerated library of primitives for deep neural networks.
* **Install Python**
    * The Python distribution can be from [Anaconda](https://www.continuum.io/), [Python Software Foundation](https://www.python.org/) or the extremely portable [WinPython](https://winpython.github.io/).
* **Install Tensorflow**
    * This guide works for TensorFlow-1.2.0. For older versions of TensorFlow, some python dependencies may vary or may not be required.
* **Install Keras**
    * This is a high-level neural networks API, written in Python and capable of running on top of either TensorFlow or Theano.

## Install Visual Studio

1. Refer to Section 1.1, Table 2 in the Nvidia CUDA installation [guide](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows) and choose a suitable version of Visual Studio.


2. Ensure that Visual Studio is installed before installing the CUDA Toolkit. <br/> :warning: **If you reverse the order of the install, you might run into problems when you compile the CUDA Samples codes.**


## Install CUDA Toolkit

1. Nvidia provides an excellent [guide](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html) for the installing of the CUDA Toolkit. Follow their guide carefully.


2. When the installation is done, build the Sample codes with Visual Studio. The solution files for the sample codes are located in `C:\ProgramData\NVIDIA Corporation\CUDA Samples\v8.0`


3. Run the `deviceQuery` test
    ```
    cd C:\ProgramData\NVIDIA Corporation\CUDA Samples\v8.0.61\bin\win64\Debug
    deviceQuery.exe
    ```
    It should show `Device 0: Your GPU model` in the first few lines and `Result = PASS` in the last line.


4. Run the `bandwidthTest`
    ```
    cd C:\ProgramData\NVIDIA Corporation\CUDA Samples\v8.0.61\bin\win64\Debug
    bandwidthTest.exe --device=0
    ```
    where `0` is the device number. If you have multiple GPUs in your system, this test should be done for each GPU. <br/>
    Ensure that the bandwidth for device to host and host to device is reasonable. :arrow_forward: **It should be around 3GB/s for PCIE-1, 6GB/s for PCIE-2 and 12G/s for PCIE-3.** <br/>
    Check out the technical spec of your GPU for the theoretical memory bandwidth. :arrow_forward: **The device to device bandwidth should be around the same order of magnitude but smaller.**


## Install cuDNN

1. Download cuDNN from [Nvidia cuDNN download page](https://developer.nvidia.com/rdp/cudnn-download). <br/>
    Unzip the folder. This will generate a `\cuda` folder with three folders `\bin`, `\include` and `\lib` folders within.


2. Copy `cudnn64_5.dll` in `cuda\bin` into `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\bin`, <br/>
    `cudnn.h` in `cuda\include` to `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\include` and <br/>
    `cudnn.lib` in `cuda\lib\x64` to `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\lib\x64`
    

## Install TensorFlow

_TensorFlow [installation page](https://www.tensorflow.org/install/install_windows) is an excellent guide. This section is largely adapted from their guide and will walk you through the process. <br/>
**We assumed your PC has no direct connection to the Internet**. If your PC is wired to the Internet, :no_entry: stop now and just follow the official TensorFlow guide._

There are several possibilities. 
* Does your PC have access to the PyPI repository such that you can do `pip install blah blah blah` with a breeze?
* Do you want to follow the best :thumbsup: practice and use a virtual environment? <br/> _The explanation below is adapted from [here](https://www.tensorflow.org/install/install_linux)._
    * Virtualenv is a virtual Python environment isolated from other Python development, incapable of interfering with or being affected by other Python programs on the same machine. During the virtualenv installation process, you will install not only TensorFlow but also all the packages that TensorFlow requires. To start working with TensorFlow, you simply need to "activate" the virtual environment. All in all, virtualenv provides a safe and reliable mechanism for installing and running TensorFlow. <br/>
    * Native pip installs TensorFlow directly on your system without going through any container system. We recommend the native pip install for system administrators aiming to make TensorFlow available to everyone on a multi-user system. Since a native pip installation is not walled-off in a separate container, the pip installation might interfere with other Python-based installations on your system. 
* Do you want to use Anaconda :snake:? <br/> _According to TensorFlow [installation page](https://www.tensorflow.org/install/install_windows),_
    * You may use conda to create a virtual environment. However, within Anaconda, we recommend installing TensorFlow with the `pip install` command, not with the `conda install` command. 
    * The conda package is community supported, not officially supported. That is, the TensorFlow team neither tests nor maintains the conda package. Use the conda package at your own risk.
* Do you want to use the highly portable WinPython? 
    * Assuming the sections above (Visual Studio, CUDA Toolkit, cuDNN) are done correctly, the use of WinPython makes deployment across different PCs easy.
    * As WinPython is pretty standalone, we will not set up a virtual environment in it.

_Note: For the instructions, take note before you execute your first `pip install ...` command. You may need to upgrade your pip._
```
pip install --upgrade pip
```

_Tip: To see which python packages you have, execute_
```
pip list
```

### If your PC has access to the PyPI repository, :smiley:
**A. Virtual environment, Anaconda**

1. Install [Anaconda-4.2.0](https://repo.continuum.io/archive/index.html) or any other Anaconda versions that ship with Python 3.5.x. 

2. Create a conda virtual environment named `my-tensorflow-env`.
    ```
    conda create -n my-tensorflow-env --clone root
    ```
    
3. Activate the virtual environment anywhere with
    ```
    activate my-tensorflow-env
    ```
    
    `(my-tensorflow-env)` will appear in your command prompt.
    
4. Install the TensorFlow package in the virtual environment
    ```
    pip install tensorflow-gpu
    ```
    
5. The virtual environment can be deactivated by
    ```
    deactivate my-tensorflow-env
    ```

**B. No virtual environment, Anaconda**

1. Install [Anaconda-4.2.0](https://repo.continuum.io/archive/index.html) or any other Anaconda versions that ship with Python 3.5.x. 

2. Install the TensorFlow package system-wide
    ```
    pip install tensorflow-gpu
    ```
    
**C. Virtual Environment, vanilla Python**

1. Install Python-3.5.x from the [Python Software Foundation](https://www.python.org/). <br/> There are three things to pay attention to. First, check the box to add python to PATH. Second, ensure that the installation path is not under the `../AppData` folder. This is because `AppData` is a hidden folder and you may have problems locating python from the command prompt later. Third, make a decision whether to install for all users. Installation for all users under `C:\Program Files\` will require the usage of command prompt (admin) to execute pip install outside the virtual environment.   

2. Install the `virtualenv` package. If this fails, try executing it under command prompt (admin).
    ```
    pip install virtualenv
    ```
    
3. Create a python virtual environment named `my-tensorflow-env`.
    ```
    virtualenv -p path-to-python-exe my-tensorflow-env
    ```
    
    where `path-to-python-exe` is the fullpath to the Python executable.
    
4. Activate the virtual environment anywhere with
    ```
    cd my-tensorflow-env\Scripts
    activate
    ```
    
    `(my-tensorflow-env)` will appear in your command prompt.
    
5. Install the TensorFlow package in the virtual environment
    ```
    pip install tensorflow-gpu
    ```
    
5. The virtual environment can be deactivated by
    ```
    deactivate my-tensorflow-env 
    ```

**D. No virtual Environment, vanilla Python**

1. Install Python-3.5.x from the [Python Software Foundation](https://www.python.org/). <br/> There are three things to pay attention to. First, check the box to add python to PATH. Second, ensure that the installation path is not under the `../AppData` folder. This is because `AppData` is a hidden folder and you may have problems locating python from the command prompt later. Third, make a decision whether to install for all users. Installation for all users under `C:\Program Files\` will require the usage of command prompt (admin) to execute pip install outside the virtual environment.

2. Install the TensorFlow package system-wide. If this fails, try executing it under command prompt (admin).
    ```
    pip install tensorflow-gpu
    ```
    
**E. WinPython**

1. Install [WinPython 3.6.x](https://winpython.github.io/). 

2. Launch the command prompt in your WinPython destination folder and install the TensorFlow package
    ```
    pip install tensorflow-gpu
    ```
    
### If your PC has no access to the PyPI repository, :grimacing:

**F. Virtual environment, Anaconda**

1. Install [Anaconda-4.2.0](https://repo.continuum.io/archive/index.html) or any other Anaconda versions that ship with Python 3.5.x. 

2. Download the required python dependencies from [PyPI](https://pypi.python.org/pypi).
    ```
    protobuf (>=3.2.0)
    markdown (==2.2.0)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    backports.weakref (==1.0rc1)
    ```
    
3. Download the TensorFlow package from [PyPI](https://pypi.python.org/pypi) or from `https://storage.googleapis.com/tensorflow/windows/gpu/tensorflow_gpu-1.2.0-cp35-cp35m-win_amd64.whl`.  

4. Create a conda virtual environment named `my-tensorflow-env`.
    ```
    conda create -n my-tensorflow-env --clone root
    ```
    
5. Activate the virtual environment anywhere with
    ```
    activate my-tensorflow-env
    ```
    
    `(my-tensorflow-env)` will appear in your command prompt.
        
6. Install the python dependencies and the TensorFlow package in the virtual environment
    ```
    pip install Downloads\protobuf-xxx.whl
    pip install Downloads\Markdown-xxx.tar.gz
    pip install Downloads\html5lib-xxx.tar.gz
    pip install Downloads\bleach-xxx.whl
    pip install Downloads\backports.weakref-xxx.whl
    pip install Downloads\tensorflow_gpu-1.xx-cp35-xxx.whl
    ```
    
7. The virtual environment can be deactivated by
    ```
    deactivate my-tensorflow-env
    ```

**G. No virtual environment, Anaconda**

1. Install [Anaconda-4.2.0](https://repo.continuum.io/archive/index.html) or any other Anaconda versions that ship with Python 3.5.x. 

2. Download the required python dependencies from [PyPI](https://pypi.python.org/pypi).
    ```
    protobuf (>=3.2.0)
    markdown (==2.2.0)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    backports.weakref (==1.0rc1)
    ```
    
3. Download the TensorFlow package from [PyPI](https://pypi.python.org/pypi) or from `https://storage.googleapis.com/tensorflow/windows/gpu/tensorflow_gpu-1.2.0-cp35-cp35m-win_amd64.whl`.     

4. Install the python dependencies and the TensorFlow package system-wide
    ```
    pip install Downloads\protobuf-xxx.whl
    pip install Downloads\Markdown-xxx.tar.gz
    pip install Downloads\html5lib-xxx.tar.gz
    pip install Downloads\bleach-xxx.whl
    pip install Downloads\backports.weakref-xxx.whl
    pip install Downloads\tensorflow_gpu-1.xx-cp35-xxx.whl
    ```
    
**H. Virtual Environment, vanilla Python**

1. Install Python-3.5.x from the [Python Software Foundation](https://www.python.org/). <br/> There are three things to pay attention to. First, check the box to add python to PATH. Second, ensure that the installation path is not under the `../AppData` folder. This is because `AppData` is a hidden folder and you may have problems locating python from the command prompt later. Third, make a decision whether to install for all users. Installation for all users under `C:\Program Files\` will require the usage of command prompt (admin) to execute pip install outside the virtual environment.

2. Download the required python dependencies from [PyPI](https://pypi.python.org/pypi).
    ```
    six (>=1.10.0)
    pyparsing (>=2.2.0)
    packaging (>=16.8)
    appdirs (>=1.4.0)
    setuptools (>=18.5.0)
    protobuf (>=3.2.0)
    markdown (==2.2.0)
    wheel (>=0.26)
    webencodings (==0.5.1)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    backports.weakref (==1.0rc1)
    numpy (>=1.11.0)
    werkzeug (>=0.11.10)
    ```

3. Download the TensorFlow package from [PyPI](https://pypi.python.org/pypi) or from `https://storage.googleapis.com/tensorflow/windows/gpu/tensorflow_gpu-1.2.0-cp35-cp35m-win_amd64.whl`. 
    
4. Install the `virtualenv` package. If this fails, try executing it under command prompt (admin).
    ```
    pip install Downloads\virtualenv-xxxx.whl
    ```
    
5. Create a python virtual environment named `my-tensorflow-env`.
    ```
    virtualenv -p path-to-python-exe my-tensorflow-env
    ```
    where `path-to-python-exe` is the fullpath to the Python executable.
    
    
6. Activate the virtual environment anywhere with
    ```
    cd my-tensorflow-env\Scripts
    activate
    ```
    
    `(my-tensorflow-env)` will appear in your command prompt.
    
7. Install the python dependencies and the TensorFlow package in the virtual environment
    ```
    pip install Downloads\six-xxx.whl
    pip install Downloads\pyparsing-xxx.whl
    pip install Downloads\packaging-xxx.whl
    pip install Downloads\appdirs-xxx.whl
    pip install Downloads\setuptools-xxx.whl
    pip install Downloads\protobuf-xxx.whl
    pip install Downloads\Markdown-xxx.tar.gz
    pip install Downloads\wheel-xxx.whl
    pip install Downloads\webencodings-xxx.whl
    pip install Downloads\html5lib-xxx.tar.gz
    pip install Downloads\bleach-xxx.whl
    pip install Downloads\backports.weakref-xxx.whl
    pip install Downloads\numpy-xxx.whl
    pip install Downloads\Werkzeug-xxx.whl
    pip install Downloads\tensorflow_gpu-1.xx-cp35-xxx.whl
    ```
    
8. The virtual environment can be deactivated by
    ```
    deactivate my-tensorflow-env 
    ```

**I. No virtual Environment, vanilla Python**

1. Install Python-3.5.x from the [Python Software Foundation](https://www.python.org/). <br/> There are three things to pay attention to. First, check the box to add python to PATH. Second, ensure that the installation path is not under the `../AppData` folder. This is because `AppData` is a hidden folder and you may have problems locating python from the command prompt later. Third, make a decision whether to install for all users. Installation for all users under `C:\Program Files\` will require the usage of command prompt (admin) to execute pip install outside the virtual environment.

2. Download the required python dependencies from [PyPI](https://pypi.python.org/pypi). 
    ```
    six (>=1.10.0)
    pyparsing (>=2.2.0)
    packaging (>=16.8)
    appdirs (>=1.4.0)
    setuptools (>=18.5.0)
    protobuf (>=3.2.0)
    markdown (==2.2.0)
    wheel (>=0.26)
    webencodings (==0.5.1)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    backports.weakref (==1.0rc1)
    numpy (>=1.11.0)
    werkzeug (>=0.11.10)
    ```

3. Download the TensorFlow package from [PyPI](https://pypi.python.org/pypi) or from `https://storage.googleapis.com/tensorflow/windows/gpu/tensorflow_gpu-1.2.0-cp35-cp35m-win_amd64.whl`. 
    
4. Install the python dependencies and the TensorFlow package system-wide. If this fails, try executing it under command prompt (admin).
    ```
    pip install Downloads\six-xxx.whl
    pip install Downloads\pyparsing-xxx.whl
    pip install Downloads\packaging-xxx.whl
    pip install Downloads\appdirs-xxx.whl
    pip install Downloads\setuptools-xxx.whl
    pip install Downloads\protobuf-xxx.whl
    pip install Downloads\Markdown-xxx.tar.gz
    pip install Downloads\wheel-xxx.whl
    pip install Downloads\webencodings-xxx.whl
    pip install Downloads\html5lib-xxx.tar.gz
    pip install Downloads\bleach-xxx.whl
    pip install Downloads\backports.weakref-xxx.whl
    pip install Downloads\numpy-xxx.whl
    pip install Downloads\Werkzeug-xxx.whl
    pip install Downloads\tensorflow_gpu-1.xx-cp35-xxx.whl
    ```
    
**J. WinPython**
1. Install [WinPython 3.6.x](https://winpython.github.io/).
 
2. Download the required python dependencies from [PyPI](https://pypi.python.org/pypi). 
    For TensorFlow-1.3,
    ```
    six (>=1.10.0)
    setuptools (>=18.5.0)
    protobuf (>=3.3.0)
    markdown (>=2.6.8)
    wheel (>=0.26)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    werkzeug (>=0.11.10)
    numpy (>=1.11.0) 
    tensorflow-tensorboard (<0.2.0, >=0.1.0)
    ```
    For TensorFlow-1.4,
    ```
    six (>=1.10.0)
    setuptools (>=18.5.0)
    protobuf (>=3.3.0)
    markdown (>=2.6.8)
    wheel (>=0.26)
    html5lib (==0.9999999)
    bleach (==1.5.0)
    werkzeug (>=0.11.10)
    numpy (>=1.11.0)
    futures (>=3.1.1)
    tensorflow-tensorboard (<0.50, >=0.40rc1)
    enum34 (>=1.1.6)
    ```


3. Download the TensorFlow package from [PyPI](https://pypi.python.org/pypi) or from `https://storage.googleapis.com/tensorflow/windows/gpu/tensorflow_gpu-1.2.0-cp35-cp35m-win_amd64.whl`. 

4. Launch the command prompt in your WinPython destination folder and install the python dependencies and the TensorFlow package
    ```
    pip install Downloads\six-xxx.whl
    pip install Downloads\pyparsing-xxx.whl
    pip install Downloads\packaging-xxx.whl
    pip install Downloads\appdirs-xxx.whl
    pip install Downloads\setuptools-xxx.whl
    pip install Downloads\protobuf-xxx.whl
    pip install Downloads\Markdown-xxx.tar.gz
    pip install Downloads\wheel-xxx.whl
    pip install Downloads\webencodings-xxx.whl
    pip install Downloads\html5lib-xxx.tar.gz
    pip install Downloads\bleach-xxx.whl
    pip install Downloads\backports.weakref-xxx.whl
    pip install Downloads\numpy-xxx.whl
    pip install Downloads\Werkzeug-xxx.whl
    pip install Downloads\tensorflow_gpu-1.xx-cp35-xxx.whl
    ```

### Validate TensorFlow installation

1. Invoke python and enter the following short program inside the python interactive shell.
    ```
    import tensorflow as tf
    hello = tf.constant('Hello, TensorFlow!')
    sess = tf.Session()
    print(sess.run(hello))
    ```
    
    If the system outputs `Hello, TensorFlow!`, then you are good to go. :v:
    
## Install Keras

_Note: You must install TensorFlow before installing Keras if you want to use TensorFlow as the backend._

### If your PC has access to the PyPI repository, :smiley:

1. In your virtual environment, non-virtual or portable environment, execute
    ```
    pip install keras
    ```

2.  Download the `numpy-1.1x.x+mkl` package from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy) and the `scipy-0.19.0` package from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy). <br/>
    Install the `numpy+mkl` package before installing the `scipy` package. <br/>
    ```
    pip install Downloads\numpy-1.1x.x+mkl-xxx.whl
    pip install Downloads\scipy-0.19.0-xxx.whl
    ```
    
    Finally, install the `keras` package.
    ```
    pip install keras
    ```
    
### If your PC has no access to the PyPI repository, :grimacing:    
    
1. Download the `numpy-1.1x.x+mkl` package from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy) and the `scipy-0.19.0` package from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy). <br/>

2. Download the additional dependencies from [PyPI](https://pypi.python.org/pypi).
    ```
    Theano (any version)
    PyYAML>=3.10
    ```
    
3. Download the `keras` package from [PyPI](https://pypi.python.org/pypi).

4. In your virtual environment, non-virtual or portable environment, install the dependencies in this order.
    ```
    pip install Downloads\numpy-1.1x.x+mkl-xxx.whl
    pip install Downloads\scipy-0.19.0-xxx.whl
    pip install Downloads\Theano-0.x.x.tar.gz
    pip install Downloads\PyYAML-3.1x.tar.gz
    ```
    
5. Finally, install the `keras` package.
    ```
    pip install Downloads\Keras-2.x.x.tar.gz
    ```
    