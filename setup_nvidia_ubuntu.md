# Guide to setup NVIDIA GPU Compute on Ubuntu 16.04

This guide explains how to setup a Ubuntu 16.04 environment to run computations on Nvidia GPUs. This is an alternative to using Ubuntu repositories to install Nvidia drivers and CUDA. <br/>

The flow is
* **Install Nvidia Driver**
    * _The CUDA Toolkit requires Nvidia proprietary drivers. Use the latest Nvidia driver instead of the one bundled with the CUDA installer._
    * This can be done with or without Secure Boot.
* **Install CUDA Toolkit**
    * _The toolkit and the sample codes will be installed._
* **Install cuDNN**
    * _This is a GPU-accelerated library of primitives for deep neural networks._
* **Configure Nvidia Settings**
    * _This allows the manual control of the GPU fan speed._


## Installing Nvidia Driver with UEFI Secure Boot Disabled

_Note: The Secure Boot function, starting from Ubuntu 16.04, requires kernel drivers to be signed. :exclamation: **This section assumes Secure Boot is disabled. It is generally not recommended to disable Secure Boot.** Refer to [this](https://askubuntu.com/questions/755238/why-disabling-secure-boot-is-enforced-policy-when-installing-3rd-party-modules) for more background._

1. Download the latest Nvidia driver from the official [Nvidia driver download page](http://www.nvidia.com/Download/index.aspx?lang=en-us). <br/> 
:warning: **This step is critical!** You might not be able to login to the desktop if a kernel upgrade breaks your current driver. <br/> 
Save the *.run* installer into a handy folder like `~/Downloads`. 


2. It is a good practice to update your Ubuntu distribution.
    ```
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get dist-upgrade
    ```


3. The default graphics driver shipped with the Ubuntu distribution is the [Nouveau](https://en.wikipedia.org/wiki/Nouveau_(software)) driver.  In order to install Nvidia drivers, Nouveau driver has to be disabled. In some cases, the installer may prompt and do this for you. However, it is better to disable and blacklist Nouveau manually to avoid future complications with Nouveau. <br/>

    Create a blacklist file
    ```
    sudo nano /etc/modprobe.d/blacklist-nouveau.conf
    ``` 

    and add the following lines at the end before saving
    ``` 
    blacklist nouveau
    options nouveau modeset=0 
    ```

    Update the initramf and reboot
    ```
    sudo update-initramfs -u
    reboot
    ```


4. After reboot, you will see a low resolution of Ubuntu. You might be bounced back to the login page when you attempt to login. This is normal. <br/>
Launch a tty instance by pressing __Ctrl+Alt+F1__. <br/>
Login with your username and password. <br/>
Ensure the Nouveau driver is not loaded.
    ```
    lsmod | grep nouveau
    ```
    The command above should return nothing.

    Stop the display manager. If this is not done, the installer will complain of a running X server and will not proceed. The default display manager in Ubuntu is `lightdm`.
    ```
    sudo service lightdm stop
    ```
    If the screen goes black, relaunch a tty instance by pressing __Ctrl+Alt+F1__.

    Navigate to the folder where the _.run_ installer is stored. Change the permission of the installer and execute it
    ```
    sudo chmod 755 NVIDIA-Linux-x86_64-375.39.run
    sudo ./NVIDIA-Linux-x86_64-375.39.run
    ```


5. The installer will be launched in a DOS mode.
    * `Please read the following LICENSE and select... ` <br/>
    Select **Accept**. 
    * If there is a prompt that says <br/>
    `There appears to already be a driver installation on your system...` <br/>
    select **Continue Installation**.
    * `The distribution-provided pre-install script failed! Are you sure you want to continue?` <br/>
    Select **Continue Installation**. 
    * If there is a prompt that says <br/>
    `Would you like to register the kernel module sources with DKMS> ...` <br/>
    Select **Yes**.
    * A status bar will display and show building kernel modules. When this is done, a prompt will say <br/> 
    `Warning: unable to find a suitable destination to install 32-bit compatibility library.` <br/>
    Select **OK**. 
    * The installation will continue with a status bar showing <br/>
    `Searching for conflict files and Installing driver..`
    * If all goes well, the last prompt will be <br/>
    `Would you like to run the nvidia-xconfig utility to automatically update your X configuration file...?` <br/>
    Select **Yes**.
    * `Your X configuration file has been successfully updated. Installation of the NVIDIA Accelerated Graphics Driver for Linux-x86_64 (version: 3XX.XX) is now complete.` <br/>
    Select **OK**. :+1:


6. Reboot and you will be able to login to your desktop. Launch a terminal and execute

    Check that Nvidia driver is installed properly with
    ```
    cat /proc/driver/nvidia/version
    ```

    or check the GPU status with
    ```
    nvidia-smi
    ```

    or check that the devices files `/dev/nvidia*` exist.
    ```
    ls -al /dev | grep nvidia
    ```
    
## Installing Nvidia Driver with UEFI Secure Boot Enabled

_Note: The Secure Boot function, starting from Ubuntu 16.04, requires kernel drivers to be signed. :exclamation: **This section assumes Secure Boot is enabled.**_ Older systems that is using Legacy Boot (not UEFI Boot) will not have the Secure Boot feature. For these systems, please refer to the section above instead.


1. Download the latest Nvidia driver from the official [Nvidia driver download page](http://www.nvidia.com/Download/index.aspx?lang=en-us). <br/> 
:warning: **This step is critical!** You might not be able to login to the desktop if a kernel upgrade breaks your current driver. <br/> 
Save the *.run* installer into a handy folder like `~/Downloads`. 


2. It is a good practice to update your Ubuntu distribution.
    ```
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get dist-upgrade
    ```

3. Create a pair of private and public keys to sign the driver. Store the keys at your home directory for convenience during installation. :warning: After the installation is completed, you should either delete the private key or store it elsewhere.
    ```
    openssl req -new -x509 -newkey rsa:2048 -keyout mykey.priv -outform DER -out mykey.der -nodes -days 36500 -subj "/CN=Nvidia Driver Key/"
    ```
    where `mykey.priv` is the name given to the private key, `mykey.der` is the name given to the public key, and `36500` states the number of days the public key will be valid for.


4. Install the `mokutil` package.
    ```
    sudo apt-get install mokutil
    ```
        
5. Add the public key to your system Machine Owner Key (MOK). You will be prompted to create a one-time password which will be used during reboot to enroll the public key.
    ```
    sudo mokutil --import mykey.der
    sudo reboot
    ```
    Refer to the graphical guide in [this](https://sourceware.org/systemtap/wiki/SecureBoot) for the key enrollment walkthrough. <br/>
    For some ASUS motherboards, the mokutil way of enrolling the public key may fail to work. We refer you to Comment 2 in this [post](https://bugzilla.redhat.com/show_bug.cgi?id=1263992) for another way that adds the public key to the system keyring via UEFI BIOS option.


6. The default graphics driver shipped with the Ubuntu distribution is the [Nouveau](https://en.wikipedia.org/wiki/Nouveau_(software)) driver.  In order to install Nvidia drivers, Nouveau driver has to be disabled. In some cases, the installer may prompt and do this for you. However, it is better to disable and blacklist Nouveau manually to avoid future complications with Nouveau. <br/>

    Create a blacklist file
    ```
    sudo nano /etc/modprobe.d/blacklist-nouveau.conf
    ``` 

    and add the following lines at the end before saving
    ``` 
    blacklist nouveau
    options nouveau modeset=0 
    ```

    Update the initramf and reboot
    ```
    sudo update-initramfs -u
    reboot
    ```

7. After reboot, you will see a low resolution of Ubuntu. You might be bounced back to the login page when you attempt to login. This is normal. <br/>
Launch a tty instance by pressing __Ctrl+Alt+F1__. <br/>
Login with your username and password. <br/>
Ensure the Nouveau driver is not loaded.
    ```
    lsmod | grep nouveau
    ```
    The command above should return nothing.

    Stop the display manager. If this is not done, the installer will complain of a running X server and will not proceed. The default display manager in Ubuntu is `lightdm`.
    ```
    sudo service lightdm stop
    ```
    If the screen goes black, relaunch a tty instance by pressing __Ctrl+Alt+F1__.

    Navigate to the folder where the _.run_ installer is stored. Change the permission of the installer and execute it
    ```
    sudo chmod 755 NVIDIA-Linux-x86_64-375.39.run
    sudo ./NVIDIA-Linux-x86_64-375.39.run --module-signing-secret-key=/path/to/your/privatekey.priv --module-signing-public-key=/path/to/your/publickey.der
    ```


8. The installer will be launched in a DOS mode.
    * `Please read the following LICENSE and select... ` <br/>
    Select **Accept**. 
    * If there is a prompt that says <br/>
    `There appears to already be a driver installation on your system...` <br/>
    select **Continue Installation**.
    * `The distribution-provided pre-install script failed! Are you sure you want to continue?` <br/>
    Select **Continue Installation**. 
    * If there is a prompt that says <br/>
    `Would you like to register the kernel module sources with DKMS> ...` <br/>
    Select **Yes**.
    * A status bar will display and show building kernel modules. When this is done, a prompt will say <br/> 
    `Warning: unable to find a suitable destination to install 32-bit compatibility library.` <br/>
    Select **OK**. 
    * The installation will continue with a status bar showing <br/>
    `Searching for conflict files and Installing driver..`
    * If all goes well, the last prompt will be <br/>
    `Would you like to run the nvidia-xconfig utility to automatically update your X configuration file...?` <br/>
    Select **Yes**.
    * `Your X configuration file has been successfully updated. Installation of the NVIDIA Accelerated Graphics Driver for Linux-x86_64 (version: 3XX.XX) is now complete.` <br/>
    Select **OK**. :+1:


9. Reboot and you will be able to login to your desktop. Launch a terminal and

    Check that Nvidia driver is installed properly with
    ```
    cat /proc/driver/nvidia/version
    ```

    or check the GPU status with
    ```
    nvidia-smi
    ```

    or check that the devices files `/dev/nvidia*` exist.
    ```
    ls -al /dev | grep nvidia
    ```
    
    Check that Secure Boot is enabled.
    ```
    sudo mokutil --sb-state
    ```
    
    :warning: **Consider deleting the private key or keep it safely elsewhere, preferably on a removable storage. This is because anyone who has access to the private key (which is carelessly stored in the home directory or folders) is free to install modules/drivers that will be authenticated by your system.**


## Installing CUDA Toolkit

_Nvidia provides an excellent [guide](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/) for the installing of the CUDA Toolkit. This section is largely adapted from their guide and will highlight some points to note. We will refer to [Nvidia guide](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/)  and walk you through the installation process. **We assumed you have completed the installation of the Nvidia driver as described earlier**._ 

1. Download the runfile installer from [Nvidia CUDA download page](https://developer.nvidia.com/cuda-downloads).


2. Perform the pre-installation actions in Section 2 of the Nvidia [guide](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/) and pay attention to <br/>
    `2.4. Verify the System has the Correct Kernel Headers and Development Packages Installed`

    If your system has an older version of the CUDA Toolkit, consider uninstalling it by 
    ```
    sudo /usr/local/cuda-X.Y/bin/uninstall_cuda_X.Y.pl
    ```
    
    where X and Y refer to the older CUDA version.

3. Skip Section 3 of the Nvidia [guide](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/) as we are installing with the runfile. Navigate to the folder containing the installer. Change the permission of the installer and execute it
    ```
    sudo chmod 755 cuda_8.0.61_linux.run
    sudo ./cuda_8.0.61_linux.run
    ```
    
4. The installer will be launched in an interactive text mode.
    * `End User License Agreement...` <br/>
    To go right to the end, press **q**.
    * `Do you accept the previously read EULA?` <br/>
    Type **Accept**.
    * `Install NVIDIA Accelerated Graphics Driver for Linux-x86_64 3XX.XX?` <br/>
    :warning: **Do not install the Nvidia driver bundled with the toolkit installer!** Type **No**.
    * `Install the CUDA 8.0 Toolkit?` <br/>
    Type **Yes**.
    * `Enter Toolkit location [default is /usr/local/cuda-8.0]` <br/>
    Press **enter** to use the default.
    * `Do you want to install a symbolic link at /usr/local/cuda?` <br/>
    Type **Yes**.
    * `Install the CUDA 8.0 Samples?` <br/>
    Type **Yes**.
    * `Enter CUDA Samples Location [default is /home/($USER_ACC)]:` <br/>
    Press **enter** to use the default.
    * It will show `Installing the CUDA Toolkit in /usr/local/cuda-8.0...` <br/>
    At the end of the installation, there should be a warning message which we should not worry about as we have already installed the Nvidia driver. <br/>
    `Incomplete installation! This installation did not install the CUDA Driver. A driver of version at least 361.00 is required for CUDA 8.0 functionality to work.`
   
   
5. Refer to Section 7.1 of the Nvidia [guide](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/) and complete the mandatory post-installation actions. Open `~/.bashrc` and add the following lines to the end
    ```
    export PATH=/usr/local/cuda-8.0/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64:$LD_LIBRARY_PATH
    ```
    
    Reload your bash profile by either executing
    ```
    source ~/.bashrc
    ```
    
    or closing all terminals and then open a new terminal.
    

6. Check the version of your CUDA Toolkit by executing this in your termimal.
    ```
    nvcc -V
    ```
    
    Navigate to CUDA_Samples folder and compile the sample codes
    ```
    cd ~/NVIDIA_CUDA-8.0_Samples
    make
    ```
    
    For faster build, run
    ```
    make -j 12
    ```
    
    where `12` after the `-j` option calls upon 12 CPU threads. For rebuild, run the following first
    ```
    make clean
    ```
    
    If there is an error message `/usr/bin/ld: cannot find -lglut`, this means that the GLUT (openGL Utility Toolkit) library is not found. Install it by running
    ```
    sudo apt-get install freeglut3-dev
    ```        
    
    If there is an error message `/usr/bin/ld: cannot find -lGL`, check that the file `libGL.so` exist and establish the necessary symbolic links.
        
        
7. Run the `deviceQuery` test
    ```
    cd NVIDIA_CUDA-8.0_Samples/bin/x86_64/linux/release
    ./deviceQuery
    ```
    It should show `Device 0: Your GPU model` in the first few lines and `Result = PASS` in the last line.
    

8. Run the `bandwidthTest`
    ```
    cd NVIDIA_CUDA-8.0_Samples/bin/x86_64/linux/release
    ./bandwidthTest --device 0
    ```
    where `0` is the device number. If you have multiple GPUs in your system, this test should be done for each GPU. <br/>
    Ensure that the bandwidth for device to host and host to device is reasonable. :arrow_forward: **It should be around 3GB/s for PCIE-1, 6GB/s for PCIE-2 and 12G/s for PCIE-3.** <br/>
    Check out the technical spec of your GPU for the theoretical memory bandwidth. :arrow_forward: **The device to device bandwidth should be around the same order of magnitude but smaller.**


## Installing cuDNN

1. Download cuDNN from [Nvidia cuDNN download page](https://developer.nvidia.com/rdp/cudnn-download). <br/>
    Decompress the files
    ```
    tar -xvf cudnn-8.0-linux-x64-v5.1-ga.tgz
    ```

    This will generate a `/cuda` folder with two folders `/include` and `/lib64` folders within. The header file is in `/include`. The so files are in `/lib64`.


2. You can copy the files either to your system directory or your CUDA directory.
    ```
    sudo cp include/cudnn.h /usr/include
    sudo cp lib64/libcudnn* /usr/lib/x86_64-linux-gnu
    ```

    or if your CUDA is installed in `/usr/local/cuda-8.0`
    ```
    sudo cp include/cudnn.h /usr/local/cuda-8.0/include
    sudo cp lib64/libcudnn* /usr/local/cuda-8.0/lib64
    ```
    
## Configuring Nvidia Settings

_This section is optional. Skip this if you do not want to play with your GPU fan speed._

1. Create a backup copy of `xorg.conf`
   ```
   sudo nvidia-xconfig
   ```
   
2. A GPU needs to have a X screen attached to it in order to control the fan speed through Coolbits. This can be done with `nvidia-settings`. You can skip this step if your PC has only 1 GPU. If your PC has more than 1 GPU, connect the non-display GPU to another monitor and create a new X-screen for it.
   ```
   sudo nvidia-settings
   ```
  
3. Open `xorg.conf`. 
   ```
   sudo nano /etc/X11/xorg.conf
   ```
   Add a line `Option "Coolbits" "4"` below the line `DefaultDepth 24` in the `"Screen"` sections. 
   ```
   Section "Screen"
        Identifier     "Screen0"
        Device         "Device0"
        Monitor        "Monitor0"
        DefaultDepth    24
        Option         "Coolbits" "4"
        Option         "Stereo" "0"
        Option         "nvidiaXineramaInfoOrder" "DFP-0"
        Option         "metamodes" "nvidia-auto-select +0+0"
        Option         "SLI" "Off"
        Option         "MultiGPU" "Off"
        Option         "BaseMosaic" "off"
        SubSection     "Display"
            Depth       24
        EndSubSection
    EndSection
    
    Section "Screen"
        Identifier     "Screen1"
        Device         "Device1"
        Monitor        "Monitor1"
        DefaultDepth    24
        Option         "Coolbits" "4"
        Option         "Stereo" "0"
        Option         "metamodes" "nvidia-auto-select +0+0"
        Option         "SLI" "Off"
        Option         "MultiGPU" "Off"
        Option         "BaseMosaic" "off"
        SubSection     "Display"
            Depth       24
        EndSubSection
    EndSection
    ```
    
4. Save the file, log out of your account and login again.

5. To adjust your GPU fan speed, launch `nvidia-settings`.
   ```
   sudo nvidia-settings
   ```
    Go to Thermal Settings under desired GPU (eg. GPU 0). There will be a slider for you to change the GPU fan speed. :v: <br/>
    :warning: **Do not set the fan speed to 0% when you are running a GPU compute instance. You will fry your GPU!**
 

### References
* https://gitlab.com/hhchin87/setup_guides/blob/master/Nvidia_guide.md
* https://antechcvml.blogspot.sg/2015/12/how-to-control-fan-speed-of-multiple.html