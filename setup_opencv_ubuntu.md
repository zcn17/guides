# OpenCV with Python3.x bindings in Ubuntu

## Download OpenCV sources

  1. Download OpenCV from [here](https://github.com/opencv/opencv). Select the desired version under `Tags`.
  2. Download OpenCV contrib for some extra modules from [here](https://github.com/opencv/opencv_contrib). Select the matching version under `Tags`.


## Install OpenCV dependencies

1. Launch the terminal and execute the following commands. If you do not have sudo or apt-get rights, please approach your sysadmin.
	```
    sudo apt-get install build-essential cmake pkg-config
    sudo apt-get install libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev
    sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
    sudo apt-get install libxvidcore-dev libx264-dev
    sudo apt-get install libgtk-3-dev
    sudo apt-get install libatlas-base-dev
    sudo apt-get install gfortran
    ```

## Setup Python Virtual Environment

1. Create a python virtual environment if you do not have one.
	```
    virtualenv -p path/to/your/python.exe path/to/your/python/virtualenv
    ```
    where  `path/to/your/python.exe` could be `/usr/bin/python3`. The destination folder `path/to/your/python/virtualenv` can also be a hidden folder such as `.my-opencv`.
    
2. Activate your virtual environment.
	```
    cd path/to/your/python/virtualenv
    source bin/activate
    ```
    
3. Install numpy in the virtual environment. This step is often missed out.
	```
    pip install numpy
    ```
    
## Configure cmake, compile OpenCV and install OpenCV

1. Make sure that you are in your virtual environment by examining your command line in a terminal. If you see the text `(my-opencv)` preceding your prompt, it means you are in the `my-opencv` virtual environment. Else, activate your virtual environment.
    ```
    cd path/to/your/python/virtualenv
    source bin/activate
    ```

2. Navigate to the `opencv` directory, create a `build` folder within it and configure `cmake`.
	```
    cd path/to/the/opencv-3.x.0
    mkdir build
    cd build
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
    	-D CMAKE_INSTALL_PREFIX=path/to/your/cmake/install/prefix \
    	-D INSTALL_PYTHON_EXAMPLES=OFF \
    	-D INSTALL_C_EXAMPLES=OFF \
    	-D OPENCV_EXTRA_MODULES_PATH=path/to/the/opencv_contrib-3.x.0/modules \
    	-D PYTHON_EXECUTABLE=path/to/your/python/virtualenv/bin/python \
        -D BUILD_opencv_matlab=OFF \
    	-D BUILD_EXAMPLES=OFF ..
    ```
    * You need to specify the path for `CMAKE_INSTALL_PREFIX`. This is the path where nearly all the OpenCV `.so` files will be installed. If you want a portable installation, set this path to a portable folder. This is recommended if you are using OpenCV **only for this virtual environment in the computer**. If you are a system administrator and you are looking for a system-wide installation, set this path to `/usr/local`.
    * You need to specify the path to the opencv_contrib-3.x.0/modules folder. <br/>
    * You need to specify the path to the python executable that resides in your python virtual environment. <br />
    * You must set BUILD_opencv_matlab=OFF if not you might get some nasty matlab generator errors. <br />
    * You must include the two dot dot (..) at the end. Otherwise cmake will complain cannot find a place to build.
    
3. After configuring cmake, check the output in the console and ensure that the `Python 3` section includes valid paths to the `Interpreter`, `Libraries`, `numpy` and `packages path`. <br />
The `Interpreter` should point to your Python3 executable in your virtual environment. <br />
The `Libraries` should point to the general Python3 Libraries, eg. `/usr/lib/x86_64-linux-gnu/libpython3.xm.so`. <br />
The `numpy` should point to your numpy installation in your virtual environment. <br />
The `packages` points to `lib/python3.x/site-packages`. When combined with the `CMAKE_INSTALL_PREFIX`, it will be the location for the compiled `cv2.so`. 

4. Compile opencv. This step will take a while. Be patient.
	```
    make -j 96
    ```
    where 96 is the number of CPU threads to use.
    
5. If there is an error, run `make clean` to flush the build, rectify the error before issuing `make` again.

6.  Install opencv.
	```
    make install
    ```
    
7. Retrieve the `cv2.so` file.
	```
    cd path/to/your/cmake/install/prefix/lib/python3.x/site-packages
    cp cv2.cpython-3.xm-x86_64-linux-gnu.so path/to/your/python/virtualenv/lib/python3.x/site-packages/cv2.so
    ```
    
## Verify the installation

1. Launch a python instance in your virtual environment and execute the following commands.
	```
    >>> import cv2
    >>> cv2.__version__
    '3.x.0'
    ```

### References
* https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv
